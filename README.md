Follow Magento best practice that described here - https://magentotherightway.com

### Install PEAR and PHP Code Sniffer ###
```
#!sh
sudo apt-get install php-pear && pear install PHP_CodeSniffer
```

###Install Webinse PHPCS Magento Rules###

Clone this repository into your PHPCS Standards directory, using the directory name of Webinse

```
#!sh
git clone git@bitbucket.org:webinse/dev_settings.git /usr/share/php/PHP/CodeSniffer/Standards/Webinse
```

You can check the standard got installed by running 

```
#!bash

phpcs -i 
```

which should show you something like this

```
#!bash

The installed coding standards are MySource, Squiz, Zend, PHPCS, Webinse and PEAR
```


### Import settings for PhpStorm ###
* Import new settings to PhpStorm (File > Import Settings(path: /usr/share/php/PHP/CodeSniffer/Standards/Webinse/settings.jar))
* Go to File > Settings
* Go to "Language and Frameworks > PHP > Code Sniffer" 
* Write path to 'phpcs' in "PHP Code Sniffer (phpcs) path:" field (standard path is "/usr/bin/phpcs")
* Search for “PHP Code Sniffer validation” and turn on this option 
* Make sure that “Webinse” is selected value in “Codding Standard” field
* Apply changes